package models;

import java.util.*;

import javax.persistence.*;

import play.db.jpa.*;
import utils.LatLng;

@Entity
public class Residence extends Model
{

  @ManyToOne
  public User user;
  
  public String residenceType;
  public String rented;
  public int	rent;
  public int	bedrooms;
  public String geolocation;
  
  public void addUser(User user)
  {
  	this.user = user;
  	this.save();
  }

	public LatLng getGeolocation()
	{
		return LatLng.toLatLng(this.geolocation);
	}
}
