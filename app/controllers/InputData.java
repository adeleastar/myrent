package controllers;


import java.util.*;

import models.*;

import org.json.simple.JSONObject;

import play.*;
import play.mvc.Before;
import play.mvc.Controller;


public class InputData extends Controller
{
  @Before
	static void checkAuthentification()
	{
	  if(session.contains("logged_in_userid") == false)
	  	Accounts.login();
	}
  
  public static void index()
  {
  	render();
  }
	
	public static void dataCapture(Residence residence)
	{
	  User user = Accounts.getCurrentUser();
    residence.addUser(user);   
    residence.save();

    Logger.info("Residence data received and saved");
    Logger.info("Residence type: " + residence.residenceType);
    Logger.info("Rented? " + residence.rented);
    
    JSONObject obj = new JSONObject();
    String value = "Congratulations. You have successfully registered your " + residence.residenceType +".";
    obj.put("inputdata", value);
    renderJSON(obj);
  }
}
